requirejs.config({
  "baseUrl": "bower_components",
  "paths": {
    "app": "../js",
    "jquery": "jquery/dist/jquery.min",
    "fullPage": "fullpage/jquery.fullPage",
    "bootstrap": "bootstrap/dist/js/bootstrap"
  },
  "shim": {
    "fullPage": ["jquery"],
    "bootstrap": ["jquery"]
  }
});

// Load the main app module to start the app
requirejs(["app/onboardapp"]);