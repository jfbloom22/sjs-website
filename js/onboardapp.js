define(["jquery", "fullPage"], function($) {
  console.log("jquery ready");
  $(function() {
    console.log("jquery dom ready");
    $("#fullpage").fullpage({
      anchors: ['firstPage', 'secondPage'],
      sectionsColor : ['#2BB5F7', '#2BB5F7'],
    });
    $("#fullpage").fadeIn();
  });
});